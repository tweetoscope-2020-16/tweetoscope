from MAP_estimation import estimator, moment
from prediction import compute_G1
import numpy as np
import json                                      # To parse and dump JSON
from kafka import KafkaConsumer, KafkaProducer   # Import Kafka consumer
import csv
import os.path

# Functions used to read and write on the csv files 
# storing datasets of (p,beta) and alpha

def write_params(file_name, params) :
    with open(file_name,'a+') as params_file:
        params_file.write(params)
    return()

def read_params(file_name):
    params_set = []
    if file_name == 'params_p_beta.csv' :                  
        for row in csv.reader(open(file_name),delimiter=",") :
            p,beta=row
            params_set.append([float(p),float(beta)])
        return params_set
    elif file_name == 'params_alpha.csv' :
        for row in csv.reader(open(file_name)) :
            alpha=row[0]
            params_set.append(float(alpha))
        return params_set



if __name__ == "__main__":


  consumer = KafkaConsumer('cascade_series',
       bootstrap_servers=['localhost:9092'],
       group_id='MAP_estimators',
       value_deserializer=lambda x: json.loads(x.decode('utf-8')))
  
  producer = KafkaProducer(bootstrap_servers = 'localhost:9092',          # List of brokers passed from the command line
       value_serializer=lambda v: json.dumps(v).encode('utf-8'),          # How to serialize the value to a binary buffer
        key_serializer=str.encode)                                        # How to serialize the key
  
  
  moment_p_beta = [ 0.02, 0.0002, 0.01, 0.001, -0.1]                      # Global variable for the prior parameters
  alpha = 2.016
  mu = 1                                                                  # Global variable for alpha
  nb_updates_prior = 1
  nb_updates_alpha = 1
  while(True) :    
    for msg in consumer:                     # Blocking call waiting for a new message
      
        if msg!= None and msg!={}:  
            msg = msg.value
            # If the message is of type series, we simply compute the MAP estimation of p,beta and G1. 
            # They are sent to the predictor through the cascade_properties topic
            if msg['type'] == 'serie':
                print("on a recu un serie")
                T_obs = msg['T_obs']
                cascade = np.floor(np.array(msg['tweets']))
                params = estimator(cascade, T_obs, prior_params = moment_p_beta, alpha = alpha, mu = mu)
                p = params[0]
                beta = params[1]
                G1 = compute_G1(cascade, p, beta, T_obs)
                msg_params = { 'type'   :   'parameters'      ,
                              'cid'    :   msg['cid']        ,
                              'msg'    :   msg['msg']        ,
                              'n_obs'  :   len(msg['tweets']),
                              'params' :   [p,beta]          , 
                              'G1'     :    G1               ,
                              'alpha'  :   alpha             }

                producer.send('cascade_properties', key = str(T_obs), value = msg_params)

            # If the message is of type params_alpha, it means we have computed a new empirical value for alpha in the collector
            elif msg['type'] == 'params_alpha':
                # We simply update the global variable alpha
                print("on a recu un alpha = " + str(msg['alpha']))
                params = str(msg['alpha']) + "\n"
                write_params('params_alpha.csv', params)

            elif msg['type'] == 'params_p_beta':
                # We add the parameters received (computed by MLE in the collector) in the set of parameters p,beta
                print("on a recu un params_p_beta p = " + str(msg["params"][0]) + "  " + str(msg["params"][1]))
                params=str(msg["params"][0]) + "," + str(msg["params"][1])+"\n"
                write_params('params_p_beta.csv', params)

            # Every 10 new parameters, we compute p and beta moments from the set of parameters and we update the prior parameters in moment_p_beta
            # Likewise, every 10 new values of alpha, we compute the mean and we save it in alpha
            if os.path.isfile('params_p_beta.csv') and len(list(csv.reader(open('params_p_beta.csv'))))>= 10*nb_updates_prior:
                    moment_p_beta = moment(read_params('params_p_beta.csv'))
                    nb_updates_prior+=1
                    print(nb_updates_prior)
                    print("actualisation prior p beta : " +str(moment_p_beta))

            if os.path.isfile('params_alpha.csv') and len(list(csv.reader(open('params_alpha.csv')))) >= 10*nb_updates_alpha:
                    alpha=np.mean(read_params('params_alpha.csv'))
                    nb_updates_alpha+=1
                    print(nb_updates_alpha)
                    print("actualisation de alpha : alpha = "+str(alpha))
