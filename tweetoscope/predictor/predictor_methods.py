import numpy as np
from math import floor
import pickle
import codecs
from sklearn.ensemble import RandomForestRegressor


np.set_printoptions(precision = 2, suppress=True) # Default precision when displaying numpy arrays 

# Global variables
alert_min_size = 150

class Predictor_Manager :

    def __init__(self,info_producer, sample_producer):
        self.predictors_dictio = {}
        self.info_producer = info_producer
        self.sample_producer = sample_producer

    def send_to_predictor(self,msg):

        if str(msg.key) in self.predictors_dictio  :
            self.predictors_dictio[str(msg.key)].process_msg(msg)
        
        else:
            new_predictor = Predictor(msg.key,self.info_producer,self.sample_producer)
            self.predictors_dictio[str(msg.key)] = new_predictor 
            new_predictor.process_msg(msg)

class Predictor:

    def __init__(self, T_obs, info_producer, sample_producer) :
        self.info_producer = info_producer
        self.sample_producer = sample_producer
        self.T_obs = T_obs        
        self.msgs_received={}
        self.model = None

    def process_msg(self,msg):
        #For a given cascade_id, there is two messages to receive :  one of type params and one of type size

        if msg.value['type'] == 'parameters' :
            # We have the values of p, beta, n_obs and G1, so we can compute n_infini
            n_infini,n_star = self.compute_n_inf(msg.value['params'],msg.value['n_obs'],msg.value['G1'],msg.value['alpha'])
            # If n_infini is higher than a decided size, we send an alert message to the topic alerts
            if n_infini > alert_min_size :
                self.send_to_kafka('alerts', cid=msg.value['cid'], msg=msg.value['msg'], n_inf=n_infini)              

            # If the message of type size has already arrived, msgs_received has an element 'cid': 'n_tot' which is the true size of the cascade

            if str(msg.value['cid']) in self.msgs_received :
                # If that is the case, we have all the data needed to compute ARE and to make the sample
                # Also, it means we have received all the infos we need for cascade 'cid', so we can remove it from msgs_received
                # It is done in send_to_kafka when the topic is 'stats'
                # print("msg_received is supposed to contain n_tot, msg_received = {}".format(self.msgs_received[str(msg.value['cid'])]))
                n_tot = self.msgs_received[str(msg.value['cid'])]
                ARE = self.compute_ARE(n_infini, n_tot)
                W = self.compute_W(n_tot, msg.value['n_obs'], msg.value['G1'], n_star, msg.value['params'][1])
                if (0.0001 <= W ):
                    self.send_to_kafka('samples', n_obs=msg.value['n_obs'], n_star=n_star, G1=msg.value['G1'], beta=msg.value['params'][1], W=W)
                self.send_to_kafka('stats', cid = msg.value['cid'], n_tot=n_tot, n_inf=n_infini, ARE=ARE)
            
            # Otherwise it means that this is the first message to arrive between the two for the cascade 'cid'
            # we simply add the element 'cid' :  n_infini in msgs_received
            # and we wait to have the true size 'n_tot' of the cascade to compute ARE
            else :
                self.msgs_received[str(msg.value['cid'])] = [n_infini, n_star, msg.value['n_obs'], msg.value['G1'], msg.value['params'][1]]

        elif msg.value['type'] == 'size' : 

            # If the message of type params has already arrived, msgs_received has an element 'cid' : [n_infini,n_star,n_obs,G1,beta]
            if str(msg.value['cid']) in self.msgs_received :
                # If that is the case, we can compute ARE and make the sample 
                # Also, it means we have received all the infos we need for cascade 'cid', so we can remove it from msgs_received
                # It is done in send_to_kafka when the topic is 'stats'
                # print("msg_received is supposed to contain a list, msg_received = {}".format(self.msgs_received[str(msg.value['cid'])]))
                n_infini = self.msgs_received[str(msg.value['cid'])][0]
                n_star = self.msgs_received[str(msg.value['cid'])][1]
                n_obs = self.msgs_received[str(msg.value['cid'])][2]
                G1 = self.msgs_received[str(msg.value['cid'])][3]
                beta = self.msgs_received[str(msg.value['cid'])][4]
                ARE = self.compute_ARE(n_infini, msg.value['n_tot'])
                W = self.compute_W(msg.value['n_tot'], n_obs, G1, n_star, beta)
                if (0.0001 <= W ):
                    self.send_to_kafka('samples',n_obs=n_obs, n_star=n_star, G1=G1, beta=beta, W=W)
                self.send_to_kafka('stats', cid=msg.value['cid'], n_tot=msg.value['n_tot'], n_inf=n_infini, ARE=ARE)

            # Otherwise it means that this message is the first of the two messages to arrive for cascade 'cid'
            # We simply add the element 'cid' : 'n_tot' in the dictionnary msgs_received 
            # and we wait to have the rest of the data we need to compute ARE
            else : 
                self.msgs_received[str(msg.value['cid'])] = msg.value['n_tot']
        
        # If the message is of type model, we simply update the model of the predictor
        elif msg.value['type'] == 'model' : 
            self.model = pickle.loads(codecs.decode(msg.value['pickle'].encode(), "base64"))

            print("model updated")
            print("Model = "+str(self.model))


    #return the prediction of the final size
    def compute_n_inf(self,params, n_obs, G1, alpha, mu = 1): 
        """
        Returns the expected total numbers of points for a set of time points

        params   -- parameter tuple (p,beta) of the Hawkes process
        alpha    -- power parameter of the power-law mark distribution
        mu       -- min value parameter of the power-law mark distribution
        """
        p,beta = params
        #branching factor
        n_star = p * mu * (alpha - 1)/(alpha - 2)
        print('n_star = {}'.format(n_star))           
        
        if self.model == None:
            if n_star >= 1 - 0.001:
                return 0,n_star

            n_inf = floor(n_obs + (G1/(1 - n_star)))
            return n_inf,n_star

        else : 
            W = self.model.predict(np.array([n_obs,G1,n_star,beta]).reshape(1,-1))

            print("W utilisé pour la prédiction")
            if n_star >= 1 - 0.001:
                return 0,n_star
            print('n_star = {}, voyons ce qui arrive avec le random forest'.format(n_star))
            n_inf = floor(n_obs + W*(G1/(1 - n_star)))
            return n_inf,n_star


    def compute_ARE(self,n_inf,n_tot):
        return np.abs(n_inf-n_tot)/n_tot

    def compute_W(self,n_tot,n_obs,G1,n_star,beta):
        print("G1 = " + str(G1))
        if( G1 == 0 ):
            return -1 
        return (n_tot-n_obs)*(1-n_star)/G1

    def send_to_kafka(self,topic,cid=None ,msg = None ,n_tot= None ,n_obs= None ,n_inf= None,
                      n_star= None ,G1= None ,beta= None ,ARE= None, W=None) :
        
        if topic == 'stats' :

            stats = { 'type'  : 'stat'      , 
                      'cid'   :  cid        ,
                      'T_obs' :  self.T_obs ,
                      'n_tot' :  n_tot      ,
                      'n_inf' :  n_inf      ,
                      'ARE'   :  ARE        }

            self.info_producer.send('stats', value = stats)
            self.msgs_received.pop(str(cid))
            print('stat sent')

        elif topic == 'alerts' :

            alert = { 'type'   : 'alert'     ,  
                      'cid'   : cid         , 
                      'msg'   : msg         , 
                      'T_obs' : self.T_obs ,  
                      'n_inf' : n_inf       }
            self.info_producer.send('alerts', value  = alert)
            print('alert sent')

        elif topic == 'samples' :
            sample = { 'type'   : 'sample'  ,  
                       'n_obs'  :  n_obs    ,
                       'G1'     :  G1       ,
                       'n_star' :  n_star   ,
                       'beta'   :  beta     ,
                       'W'      :  W        }
            self.sample_producer.send('samples', key = str(self.T_obs), value  = sample)
            print('sample sent') 

