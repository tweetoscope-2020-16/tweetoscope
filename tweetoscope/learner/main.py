import learner_methods as lm
import csv
import json                                      # To parse and dump JSON
from kafka import KafkaConsumer, KafkaProducer   # Import Kafka consumer
from sklearn.ensemble import RandomForestRegressor
import pickle
import codecs


if __name__ == "__main__":


    consumer = KafkaConsumer('samples', bootstrap_servers=['localhost:9092'],   
               value_deserializer=lambda x: json.loads(x.decode('utf-8')),
               key_deserializer= lambda v: v.decode())
  
    producer = KafkaProducer(bootstrap_servers = 'localhost:9092',                     # List of brokers passed from the command line
               value_serializer=lambda v: json.dumps(v).encode('utf-8'),
               key_serializer=str.encode)                                              # How to serialize the key
    
    T_obs = []                                   # List to store observation windows
    nb_updates = 1
    while(True) :    
        for msg in consumer:                     # Blocking call waiting for a new message
        
            if msg!= None and msg!={}:  
                # If the sample has been computed for a new observation window, we save its value in T_obs
                if msg.key not in T_obs :
                    print("adding T_obs = {} to the list of observation windows".format(msg.key))
                    T_obs.append(msg.key)

                # We save the sample in the dataset.csv file
                print("sample received for T_obs = {}".format(msg.key))
                lm.save_sample(msg)
                
                # Every 50 samples we refit the random forest and sen it to the predictor through the topic models
                for T in T_obs :
                    if len(list(csv.reader(open('dataset_'+str(T)+'.csv')))) >= 50*nb_updates :
                        print("20 new samples received, training random forest for T  = {}".format(T))
                        nb_updates += 1
                        X,y = lm.get_training_dataset(T)
                        randomForest = RandomForestRegressor(n_estimators=100, min_samples_split=6, min_samples_leaf = 3, max_features=3, bootstrap=True, n_jobs=-1,random_state=0)
                        randomForest.fit(X,y)
                        pickled = codecs.encode(pickle.dumps(randomForest), "base64").decode()
                        msg_random_forest = { 'type'    :  'model'      ,
                                              'pickle' :  pickled  }

                        producer.send('cascade_properties', key = str(T), value = msg_random_forest)
                        print("model sent for T_obs =  {}".format(T))
                        




