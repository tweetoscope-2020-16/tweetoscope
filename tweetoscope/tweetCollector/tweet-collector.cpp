#include "tweetCollector.hpp"
#include <functional>
// Compile with : g++ -o tweet-collector -O3 -Wall -std=c++17 `pkg-config --libs --cflags gaml cppkafka` -lpthread tweet-collector.cpp
// Execute with : ./tweet-collector parameterfile

int main(int argc, char* argv[]) {

  if(argc != 2) {
    std::cout << "Usage : " << argv[0] << " <config-filename>" << std::endl;
    return 0;
  }
  tweetoscope::params::collector params(argv[1]);
  std::cout << std::endl
        << "Parameters : " << std::endl
        << "----------"    << std::endl
        << std::endl
        << params << std::endl
        << std::endl;


    // Create the tweet consumer config
  cppkafka::Configuration consumer_config = {
      { "bootstrap.servers", params.kafka.brokers},
      { "auto.offset.reset", "earliest" },
      { "group.id", "myOwnPrivateGroup" }
  };
  
  // Create the tweet consumer
  cppkafka::Consumer tweet_consumer(consumer_config);
  tweet_consumer.subscribe({params.topic.in});

  // Create the producer config 
  cppkafka::Configuration producer_config = {
      { "bootstrap.servers", params.kafka.brokers }
  };

  // Create the cascade producer
  cppkafka::Producer cascade_producer(producer_config);
  
  // Send messages to the cascade_series topic
  cppkafka::MessageBuilder cascade_series_builder(params.topic.out_series);

  // Send messages to the cascade_properties topic
  cppkafka::MessageBuilder cascade_properties_builder(params.topic.out_properties);

  std::function<void(const std::string&, const std::string&)> send_cascade_properties = 
  [&cascade_properties_builder, &cascade_producer](std::string key, std::string value) -> void {
    cascade_properties_builder.key(key);
    cascade_properties_builder.payload(value);
    cascade_producer.produce(cascade_properties_builder);
  };

  std::function<void(const std::string&, const std::string&)> send_cascade_series = 
  [&cascade_series_builder, &cascade_producer](const std::string& key, const std::string& value) -> void {
    cascade_series_builder.key(key);
    cascade_series_builder.payload(value);
    cascade_producer.produce(cascade_series_builder);
  };

  // Construction of Processor by source
  tweetoscope::tweet_collector::Processors_by_sources processor_by_source(params.times.terminated, params.times.observation,params.cascade.min_cascade_size,
                                                                          send_cascade_series, send_cascade_properties );

  // Construction of Params_magnitude, which deals with the estimation of magnitude's parameter alpha
  tweetoscope::params_estimation::Params_magnitudes_set params_magnitude(send_cascade_series);


  while(true) {
    
    auto msg = tweet_consumer.poll();
    
    if( ((bool)msg) && ! msg.get_error()) {
      
      tweetoscope::tweet twt;
      // auto key = tweetoscope::cascade::idf(std::stoi(msg.get_key()));
      auto istr = std::istringstream(std::string(msg.get_payload()));
      istr >> twt;
      auto cid = tweetoscope::cascade::idf(std::stoi(twt.info.substr(8,twt.info.size())));
      //Process du tweet ici
      processor_by_source.add_tweet(twt,cid);
      params_magnitude += twt.magnitude;
   
    }
  }
}