echo "deleting tweets topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic tweets

echo "deleting cascade_properties topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic cascade_properties

echo "deleting cascade_series topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic cascade_series

echo "deleting stats topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic stats

echo "deleting alerts topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic alerts

echo "deleting samples topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic samples
