import predictor_methods
import json                       # To parse and dump JSON
from kafka import KafkaConsumer, KafkaProducer   # Import Kafka consumer


if __name__ == "__main__":


    consumer = KafkaConsumer(
    'cascade_properties',
    bootstrap_servers=['localhost:9092'],
    value_deserializer=lambda x: json.loads(x.decode('utf-8')),
    key_deserializer= lambda v: v.decode())

    info_producer = KafkaProducer(
    bootstrap_servers = 'localhost:9092',                       # List of brokers passed from the command line
    value_serializer=lambda v: json.dumps(v).encode('utf-8'))   # How to serialize the value to a binary buffer
    
    sample_producer = KafkaProducer(
    bootstrap_servers = 'localhost:9092',                       # List of brokers passed from the command line
    value_serializer=lambda v: json.dumps(v).encode('utf-8'),   # How to serialize the value to a binary buffer
    key_serializer=str.encode)                                  # How to serialize the key
    
    predictor_manager = predictor_methods.Predictor_Manager(info_producer,sample_producer)


    while(True) : 

      for msg in consumer:                     # Blocking call waiting for a new message
        if msg!= None and msg!={}:  
            predictor_manager.send_to_predictor(msg)