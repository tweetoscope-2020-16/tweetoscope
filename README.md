The git is separated in three branches, the branch master to pull for local developpement,the branch compose for deployment with docker-compose, and the branch cluseter for the deployement on a kubernetes cluster.

In the branch master, you already have dependencies you need to work on the project, which are : cppkafka, gaml & libsvm.
You will still need to download kafka on your computer. On this branch, we provide useful shells scripts kafka.sh,topicsCreation.sh and topicsDeletion.sg to start and stop kafka as well as creating and deleting the required topics.

When working with the branch master, you will have to change paths indicated in several files to the paths where you downloaded the project. You will also have to compile the c++ codes for your machine.
Those files are  :

1.  tweetoscope/tweetGenerator/params.config
2.  tweetoscope/tweetCollector/parameterfile
3.  tweetoscope/monitor/main.py
4.  tweetoscope/dashboard/main.py


If using Loki to visualize your results, you will find the docker-compose file and its dependencies in the Loki folder of each branch.

For the branch master, as for the files mentioned above, you will have to change the path indicated for the volume in the Loki/docker-compose.yaml file to your local path if your are running the pipeline locally.

If you want to run the pipeline in containers on your machine, you should use the compose branch.
There you can find dockerfiles and docker-compose files to run zookeeper,kafka,the entire pipeline and loki in containers on your machine. Everything is already set-up, you can just use docker-compose. The code is available if you want to make some changes.
The containers running the dashboard,the monitor and loki share the log files through the logs-vol volume instead of the local directory.

To deploy the project on a Kubernetes cluster, use files of the folder kubernetes of the branch cluster.
It doesn't really work because somehow we cannot manage to link the kafka broker running in the kafka pod with the rest of the pipeline.If this is fixed, just using "kubectl -n <your_namespace> apply -f <deployment_file_name>" should work. 
The code is available if you want to make some changes.
