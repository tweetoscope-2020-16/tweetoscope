#include <cppkafka/cppkafka.h>
#include <cstdlib>
#include <iostream>
#include <fstream>

// Compile with : g++ -o monitor -O3 -Wall -std=c++17 `pkg-config --libs --cflags gaml cppkafka` -lpthread main.cpp

int main(int argc, char const *argv[])
{

    cppkafka::Configuration config = {
        { "bootstrap.servers", "localhost:9092" },
	    { "auto.offset.reset", "earliest" },
	    { "group.id", "myOwnPrivateCppGroup" }
    };

    cppkafka::Consumer consumer(config);
    consumer.subscribe({"stats"}); //topic ecouté


    // std::ofstream fichierlog("/var/log/kafka/stats.log"); //dossier où enregistrer le fichier log.log
    std::ofstream fichierlog("/home/adrien/Documents/SDI/SD9/Tweetoscope/Loki/stats.log");

    if(fichierlog){ //verification que l'ouverture du fichier fonctionne 

        while(true) {
	    auto msg = consumer.poll();
	        if(msg && ! msg.get_error()) {
		    auto message = std::string(msg.get_payload());
		    fichierlog << message << std::endl;
	        }
        }
    }

    else {
    std::cout << "ERREUR: Impossible d'ouvrir le fichier." << std::endl;
    } 

    /* code */
    return 0;
}


    // char *twittoPath(getenv("TWEETOSCOPE"));
    // if (twittoPath == NULL)
    // {
    //     std::cout << "$TWEETOSCOPE is not set!" << std::endl;
    // }
    // else
    // {
    //     std::cout << "$TWEETOSCOPE is set to '" << twittoPath << "'" << std::endl;
    // }
    // std::cout << twittoPath<< std::endl;