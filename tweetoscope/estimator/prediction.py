import numpy as np
def compute_G1(history, p, beta, t):
    """
    Returns the expected total numbers of points for a set of time points
    
    params   -- parameter tuple (p,beta) of the Hawkes process
    history  -- (n,2) numpy array containing marked time points (t_i,m_i)  
    t        -- current time (i.e end of observation window)
    """
    
    history[:,0] = history[:,0] - history[0,0]
    return p * sum( history[:, 1] * np.exp( -beta * (t - history[:, 0] )) )