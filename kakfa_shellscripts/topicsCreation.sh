echo "deleting tweets topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic tweets
echo "creating tweets topic"
$KAFKA_PATH/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic tweets

echo "deleting cascade_series topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic cascade_series
echo "creating cascade_series topic"
$KAFKA_PATH/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 2 --config retention.ms=3600000 --topic cascade_series

echo "deleting cascade_properties topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic cascade_properties
echo "creating cascade_properties topic"
$KAFKA_PATH/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic cascade_properties

echo "deleting stats topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic stats
echo "creating stats topic"
$KAFKA_PATH/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic stats

echo "deleting alerts topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic alerts
echo "creating alerts topic"
$KAFKA_PATH/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic alerts

echo "deleting samples topic if it exists"
$KAFKA_PATH/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic samples
echo "creating samples topic"
$KAFKA_PATH/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic samples
