# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/examples/metadata_example.cpp" "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/build/examples/CMakeFiles/metadata.dir/metadata_example.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ALL_NO_LIB"
  "BOOST_PROGRAM_OPTIONS_DYN_LINK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../examples/../include"
  "../src/../include/cppkafka"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/build/src/CMakeFiles/cppkafka.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
