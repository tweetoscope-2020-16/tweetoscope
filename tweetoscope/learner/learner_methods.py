import csv
from sklearn.model_selection import cross_validate


def save_sample(msg) : 
    sample = msg.value
    with open('dataset_'+str(msg.key)+'.csv','a+') as dataset:
        dataset.write(str(sample['n_obs']) + "," + str(sample['G1']) + "," + str(sample['n_star']) + "," + str(sample['beta']) + "," + str(sample['W'])+"\n")
    return()


def get_training_dataset(T_obs):
    X,y = [],[]
                 
    for row in csv.reader(open('dataset_'+str(T_obs)+'.csv'),delimiter=",") :
        n_obs,G1,n_star,beta,W=row
        X.append([int(n_obs),float(G1),float(n_star),float(beta)])
        y.append(float(W))
    return X,y

# def find_best_params(X, y) : 