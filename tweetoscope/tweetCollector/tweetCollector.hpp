#pragma once
#include "tweetoscopeCollectorParams.hpp"
#include <memory>
#include <string>
#include <map>
#include <list>
#include <iostream>
#include <boost/heap/binomial_heap.hpp>
#include <cppkafka/cppkafka.h>
#include <math.h>
#include <fstream>

namespace tweetoscope {

    using timestamp = std::size_t;
    namespace source {
        using idf = std::size_t;
    }
    namespace cascade {
        using idf = std::size_t;
    }

    struct tweet {
        std::string type = "";
        std::string msg  = "";
        timestamp time   = 0;
        double magnitude = 0;
        source::idf source = 0;
        std::string info = "";

    };
    std::string cascade_to_string(std::list<tweet> cascade){
        std::string tweets = "[ ";
        for(auto it = cascade.begin() ; it!=cascade.end() ; ++it){
            tweets += "["+ std::to_string(it->time) + "," + std::to_string(it->magnitude) + "], ";
        }
        return tweets.substr(0, tweets.size() - 2 ) + " ]";
    }

    inline std::string get_string_val(std::istream& is) {
        char c;
        is >> c; // eats  "
        std::string value;
        std::getline(is, value, '"'); // eats tweet", but value has tweet
        return value; 
    }

    inline std::istream& operator>>(std::istream& is, tweet& t) {
        // A tweet is  : {"type" : "tweet"|"retweet", 
        //                "msg": "...", 
        //                "time": timestamp,
        //                "magnitude": 1085.0,
        //                "source": 0,
        //                "info": "blabla"}
        std::string buf;
        char c;
        is >> c; // eats '{'
        is >> c; // eats '"'
        while(c != '}') { 
        std::string tag;
        std::getline(is, tag, '"'); // Eats until next ", that is eaten but not stored into tag.
        is >> c;  // eats ":"
        if     (tag == "type")    t.type = get_string_val(is);
        else if(tag == "msg")     t.msg  = get_string_val(is);
        else if(tag == "info")    t.info = get_string_val(is);
        else if(tag == "t")       is >> t.time;
        else if(tag == "m")       is >> t.magnitude;
        else if(tag == "source")  is >> t.source;
        is >> c; // eats either } or ,
        if(c == ',')
            is >> c; // eats '"'
        } 
        return is;
    }
    
  
    // Namespace relatif au calcul du prior de (p,beta) et de (mu, alpha)
    // mu étant la valeur de la magnitude minimale, 
    // elle sera presque toujours égale à 1, on fait donc les calculs en supposant que mu=1
    namespace params_estimation{
        // Class which deals with the estimation of alpha
        struct Params_magnitudes_set{
            // Atributes
            std::list<int> magnitudes;
            // int nb_magnitudes;
            double update; // Number of update of alpha  start at 1
            double alpha;
            std::function<void(const std::string&, const std::string&)> send_cascade_series;
            
            // Constructor
            Params_magnitudes_set(std::function<void(const std::string&, const std::string&)> send_cascade_series) 
                                : send_cascade_series(send_cascade_series), alpha(2.016),update(1){}

            
            // Take one more magnitude, and send the estimated parameters every 1000 call               
            void operator+=(const int& magnitude){
                if (magnitude >= 10 ){
                magnitudes.push_back(magnitude);
                }
                // ++nb_magnitudes;
                if (magnitudes.size()> 1000){
                    std::cout << "1000 magnitudes bah didonc" << std::endl;
                    // update of alpha
                    int sum = 0;
                    // La ligne suivante marche pas correctement, sum est négatif alors que tout les *ptr sont positifs
                    for(auto ptr = magnitudes.begin() ; ptr != magnitudes.end() ; ptr++ ) {sum += log(*ptr/10); } 
                    alpha = alpha * (update -1) + (1 + double(magnitudes.size())/(sum)) ;
                    alpha = alpha / update;
                    
                    std::cout << "log_sum = " << sum << std::endl;  // la preuve
                    std::cout << "alpha_MLE = " << alpha << std::endl;
                    if (alpha > 2){      
                        // Conversion en json  
                        std::string json = "{ \"type\" : \"params_magnitude\", "
                                        "\"alpha\" : " + std::to_string(alpha) +  " }";                    
                        // send params
                        send_cascade_series("",json);
                    }
                    // erase magnitudes
                    magnitudes.clear();
                    update = std::min(update + 1, 10.0) ;
                }
            }
        };

        // Estimation MLE of p,beta of the given cascade, the program MLE_estimation sends it to cascade_series if it's correct
        void estimate_and_send(std::list<tweet> cascade){
            std::string cascadeString = "python3 MLE_estimation.py \"";
            cascadeString += cascade_to_string(cascade) + "\"";
            
            system(cascadeString.c_str());
        }
    }

    // Namespace relatif au tweet Collecteur
    namespace tweet_collector{
       
        struct Cascade;

        using weak_cascade = std::weak_ptr<Cascade>;
        using shared_cascade  = std::shared_ptr<Cascade>;

        // This is the comparison functor for boost queues.
        struct shared_cascade_comparator {
            bool operator()(shared_cascade c1, shared_cascade c2) const; //Defined later
        };

        //Defining the priority_queue type
        using priority_queue = boost::heap::binomial_heap<shared_cascade,
                                boost::heap::compare<shared_cascade_comparator>>;

        struct Cascade{
            std::list<tweet> list;
            size_t cascade_id;
            priority_queue::handle_type location; 
            Cascade(const tweet& twt,size_t id){
                list.push_back(twt);
                this -> cascade_id = id;
            }
        };
        
        // This is the comparison functor for boost queues.       
        bool shared_cascade_comparator::operator()(shared_cascade c1, shared_cascade c2) const{
            return (*c1).list.back().time > (*c2).list.back().time;                            // Optimisé?
        }
    
        struct Processor{

            //Attributes
            std::map<cascade::idf, weak_cascade> symbol_table;   //Symbol Table to find a cascade with idf
            std::map<timestamp,std::queue<weak_cascade>> FIFOs;  //FIFOS for computing partial cascades
            priority_queue priorityQueue;                        //Priority queue for ending cscades
            std::function<void(const std::string&, const std::string&)> send_cascade_series;
            std::function<void(const std::string&, const std::string&)> send_cascade_properties;
            Processor() = default;

            //Constructor
            Processor(std::vector<std::size_t> time_observation, 
                      std::function<void(const std::string&, const std::string&)> send_cascade_series,
                      std::function<void(const std::string&, const std::string&)> send_cascade_properties) :
                send_cascade_properties(send_cascade_properties),
                send_cascade_series    (send_cascade_series){    
                for(auto it = time_observation.begin() ; it != time_observation.end() ; it++){
                auto pair = std::make_pair<size_t,std::queue<weak_cascade>>(static_cast<size_t>(*it),std::queue<weak_cascade>());
                FIFOs.insert(pair);
                }
            }

            
            void process(tweet twt, const cascade::idf& cascade_id){
                if(auto it = symbol_table.find(cascade_id); it != symbol_table.end()){// *it = {idf, Cascade}.
                    //Let's see if it is alive
                    if (auto spt = it->second.lock()) {
                        (*spt).list.push_back(twt);                                   //cascade idf exists, we insert twt 
                        priorityQueue.decrease(spt->location,spt);
                    }
                    //cascade expired, we remove it
                    else{symbol_table.erase(cascade_id);}                                               
                }    
                else {//la cascade n'existe pas, on la rajoute si le tweet est de type tweet
                    if (twt.type == "tweet"){ 
                        
                        //On crée un shared ptr sur une nouvelle cascade qu'on ajoute à priority_queue
                        shared_cascade sharedPtr = std::make_shared<Cascade>(twt, cascade_id);                            
                        sharedPtr->location=priorityQueue.push(sharedPtr);

                        //On ajoute un weak ptr a la table symbole
                        auto pair = std::make_pair<size_t,weak_cascade>(static_cast<size_t>(cascade_id), sharedPtr);
                        symbol_table.insert(pair);
                        
                        // On ajoute un weak ptr aux queues dans FIFOs
                        for(auto ptr = FIFOs.begin() ; ptr != FIFOs.end() ; ptr++){
                            ptr->second.push(sharedPtr);
                            
                        }                          
                    }  
                }              
            };
            
                
            void send_cascade( const size_t& times_terminated, size_t time, const size_t& min_cascade_size){//time = tweet.time

                //Complete cascade
                while(priorityQueue.size() != 0 && time - (*priorityQueue.top()).list.back().time  > times_terminated ){

                    Cascade cascade = *(priorityQueue.top());
                    priorityQueue.pop();
                    if(cascade.list.size() > min_cascade_size){
                        size_t cascade_id = cascade.cascade_id;
                
                        //Send the final size and the last time to cascade_properties
                        std::string json = "{ \"type\" : \"size\""
                                            ", \"cid\" : " + std::to_string(cascade_id) +  
                                          ", \"n_tot\" : " + std::to_string(cascade.list.size()) + 
                                          ", \"t_end\" : " + std::to_string(time) + " }";
                        // Send for each observation time
                        for(auto it = FIFOs.begin(); it!=FIFOs.end(); ++it){ send_cascade_properties(std::to_string(it->first), json);}

                        // Estimation MLE of p,beta and send to cascade_series if it's correct
                        params_estimation::estimate_and_send(cascade.list);
                    }

                }
                
                //Partial cascades
                for(auto ptr = FIFOs.begin() ; ptr != FIFOs.end() ; ptr++){ //on parcourt le dictionnaire FIFOs
                    if(ptr->second.size()!=0){
                        auto spt = (ptr->second).front().lock();
                        while(((ptr->second).front().expired()) || time - (*spt).list.front().time  > ptr->first){
                            //ptr->second est une file d'attente de weak pointer de Cascade
                            if(!(ptr->second).front().expired()){
                                // envoyer la cascade sur kafka
                                ptr->second.pop();
                                Cascade cascade = *spt;
                                
                                //Computation of the size of the partial cascade
                                if (cascade.list.end()->time - cascade.list.begin()->time > ptr->first) {cascade.list.pop_back();}
                                size_t cascade_size = cascade.list.size();
                                //The cascade is posted on the kafka topic only if it has more than min_cascade_size tweets
                                if(cascade_size >= min_cascade_size){
                                    std::string json = "{ \"type\" : \"serie\""
                                                      ", \"cid\" : " + std::to_string(cascade.cascade_id) +
                                                      ", \"msg\" : \"" + cascade.list.begin()->msg + 
                                                    "\", \"T_obs\" : " + std::to_string(ptr->first) +   
                                                     ", \"tweets\" : " + cascade_to_string(cascade.list) + " }";
                                    
                                    send_cascade_series("", json);
                                }
                            }
                            else{
                                ptr->second.pop();
                            }    
                            if(ptr->second.size() !=0){                        
                                spt = (ptr->second).front().lock();
                            }
                            else{break;}
                        }
                    }                       
                }                     
            }       
        };

            
        //This struct mangages and runs the processors
        //Each processor is linked to a fixed source
        struct Processors_by_sources{
            //Map (key = source, value = Processor)
            std::map<source::idf ,Processor> map;

            //Terminated time and observations times
            size_t times_terminated;
            std::vector<size_t> times_observations;

            //Minimum cascade size :
            size_t min_cascade_size;
            
            //
            std::function<void(const std::string&, const std::string&)> send_cascade_series;
            std::function<void(const std::string&, const std::string&)> send_cascade_properties;

            //Constructor
            Processors_by_sources(const size_t& times_term,const std::vector<std::size_t>& times_obs, const std::size_t& min_cascade_size,
                                  std::function<void(const std::string&, const std::string&)> send_cascade_series,
                                  std::function<void(const std::string&, const std::string&)> send_cascade_properties )
                : times_terminated (times_term) , times_observations(times_obs), min_cascade_size(min_cascade_size),
                 send_cascade_series(send_cascade_series), send_cascade_properties(send_cascade_properties) {};

            
            void add_tweet(tweet twt,cascade::idf cascade_id){

                //We check if there is already a processor managing tweets from the source of the tweet received
                if(auto it = map.find(twt.source); it != map.end()){ // *it = {source, Processor}
                        //The processor managing the source exists, it is used to process the tweet
                        it->second.process(twt, cascade_id);
                        //And send cascades to the kafka topics if needed
                        it->second.send_cascade(times_terminated, twt.time, min_cascade_size);
                }

                else {//The processor managing the source of the received tweet doesn't exist yet
                      //It is created if the tweet received is of type "tweet", e.g if the tweet is the start of a cascade 
                    if (twt.type == "tweet"){ 
                        //The processor is created and added to the processor list
                        Processor new_processor = Processor(times_observations,send_cascade_series,send_cascade_properties);
                        auto pair = std::make_pair<size_t,Processor>(static_cast<size_t>(twt.source),static_cast<Processor>(new_processor));
                        map.insert(pair);
                        //Then it processes the tweet and eventually send cascades
                        map[twt.source].process(twt, cascade_id);
                        map[twt.source].send_cascade(times_terminated, twt.time, min_cascade_size);
                        
                    }                    
                }
            }
        };
    }
};


 