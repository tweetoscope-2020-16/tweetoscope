# Install script for directory: /home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xHeadersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cppkafka" TYPE FILE FILES
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/buffer.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/clonable_ptr.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/configuration.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/configuration_base.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/configuration_option.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/consumer.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/cppkafka.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/error.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/event.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/exceptions.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/group_information.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/header.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/header_list.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/header_list_iterator.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/kafka_handle_base.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/logging.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/macros.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/message.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/message_builder.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/message_internal.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/message_timestamp.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/metadata.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/producer.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/queue.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/topic.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/topic_configuration.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/topic_partition.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/topic_partition_list.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xHeadersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cppkafka/utils" TYPE FILE FILES
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/utils/backoff_committer.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/utils/backoff_performer.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/utils/buffered_producer.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/utils/compacted_topic_processor.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/utils/consumer_dispatcher.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/utils/poll_interface.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/utils/poll_strategy_base.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/utils/roundrobin_poll_strategy.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xHeadersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cppkafka/detail" TYPE FILE FILES
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/detail/callback_invoker.h"
    "/home/adrien/Documents/SDI/SD9/Tweetoscope/cppkafka/include/cppkafka/detail/endianness.h"
    )
endif()

