import sys
import ast
import numpy as np
import scipy.optimize as optim
import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer, KafkaProducer   # Import Kafka consumer


if __name__ == "__main__":

    ## We define the function we are going to use to make te MLE estimation of p and beta (using complete cascades) for the parameters of the prior

    def loglikelihood(params, history):
        """
        Returns the loglikelihood of a Hawkes process with exponential kernel
        computed with a linear time complexity
            
        params   -- parameter tuple (p,beta) of the Hawkes process
        history  -- (n,2) numpy array containing marked time points (t_i,m_i)  
        t        -- current time (i.e end of observation window)
        """

        p,beta = params    
        
        if p <= 0 or p >= 1 or beta <= 0.: return -np.inf
        
        n=len(history)
        tis=np.zeros(n+1)
        tis[:n]=history[:,0]
        tis[n]= tis[n-1] + 1000
        mis=history[:,1]
        Ais=np.zeros(n+1)
        
        for i in range(1,n+1):
            Ais[i]=np.exp(-beta*(tis[i]-tis[i-1])) * (mis[i-1]+Ais[i-1])
            
        LL=(n-1)*np.log(p*beta)+np.sum(np.log(Ais[1:n]))-p*(np.sum(mis)-Ais[n])

        return LL

    def prediction(params, history, alpha = 2.016, mu = 1):
        """
        Returns the expected total numbers of points for a set of time points
        
        params   -- parameter tuple (p,beta) of the Hawkes process
        history  -- (n,2) numpy array containing marked time points (t_i,m_i)  
        alpha    -- power parameter of the power-law mark distribution
        mu       -- min value parameter of the power-law mark distribution
        t        -- current time (i.e end of observation window)
        """
        t = history[-1,0]+1000
        p,beta = params
        #branching factor
        nstar = p * mu * (alpha - 1)/(alpha - 2)
        
        if nstar >= 1:
            return -1
        # G1
        G1 = p * sum( history[:, 1] * np.exp( -beta * (t - history[:, 0] )) )
        #n
        n = len( history[:, 1] )
        #Ninf
        Ninf = n + G1/(1 - nstar)
        
        return Ninf


    def compute_MLE(history, alpha = 2.016, mu =1,
                    init_params=np.array([0.0001, 1./60]), 
                    max_n_star = 1., display=False):
        """
        Returns the pair of the estimated loglikelihood and parameters (as a numpy array)

        history     -- (n,2) numpy array containing marked time points (t_i,m_i)  
        t           -- current time (i.e end of observation window)
        alpha       -- power parameter of the power-law mark distribution
        mu          -- min value parameter of the power-law mark distribution
        init_params -- initial values for the parameters (p,beta)
        max_n_star  -- maximum authorized value of the branching factor (defines the upper bound of p)
        display     -- verbose flag to display optimization iterations (see 'disp' options of optim.optimize)
        """
        
        # Define the target function to minimize as minus the loglikelihood
        target = lambda params : -loglikelihood(params, history)
        
        EM = mu * (alpha - 1) / (alpha - 2)
        eps = 1.E-8

        # Set realistic bounds on p and beta
        p_min, p_max       = eps, max_n_star/EM - eps
        beta_min, beta_max = 1/(3600. * 24 * 10), 1/(60. * 1)
        
        
        # Define the bounds on p (first column) and beta (second column)
        bounds = optim.Bounds(
            np.array([p_min, beta_min]),
            np.array([p_max, beta_max])
        )
        
        # Run the optimization
        res = optim.minimize(
            target, init_params,
            method='Powell',
            bounds=bounds,
            options={'xtol': 1e-8, 'disp': display}
        )
        
        # Returns the loglikelihood and found parameters
        return((res.x[0], res.x[1]))

    ## Main program starts here

    producer = KafkaProducer(
    bootstrap_servers = 'localhost:9092',                     # List of brokers passed from the command line
    value_serializer=lambda v: json.dumps(v).encode('utf-8'), # How to serialize the value to a binary buffer
    key_serializer=str.encode                                 # How to serialize the key
    )
    
    alpha = 2.016
    mu = 1
    # This is where we get the full cascade from the c++ tweet-collector program

    cascade = np.array(ast.literal_eval(sys.argv[1])).transpose()

    n_tot = len(cascade)
    threshold = 0.2
    p, beta = compute_MLE(cascade, alpha = alpha, mu = mu)
    N_inf = prediction([p,beta],cascade, alpha = alpha, mu = mu)

    if n_tot != 0 and abs(N_inf - n_tot)/n_tot < threshold and  p * mu * (alpha - 1) / (alpha - 2) < 1:
        msg_params = {'type'   :   'params_p_beta'   ,
                      'params' :    [p , beta ]      }
            
        producer.send('cascade_series', key = "", value = msg_params)
        print("bon hawkes")
    else :
        print("N_inf - n_tot = {}".format(abs(N_inf - n_tot)))
        if n_tot != 0 :
            print("(N_inf - n_tot) / n_tot = {}".format(abs(N_inf - n_tot)/n_tot))
        print("n_star = {}".format( p * mu * (alpha - 1) / (alpha - 2) ))
    producer.flush()
        


